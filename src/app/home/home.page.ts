import { Component,OnInit } from '@angular/core';
import {Clipboard } from '@angular/cdk/clipboard';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  ngOnInit(){
   
  }
  constructor(private clipboard : Clipboard) {}

  copytext(){
    let orderid;
    orderid = '123456780';
    this.clipboard.copy(orderid);
  }

}
